﻿using System;
using System.IO;
using System.Net;
using Newtonsoft.Json;

namespace RegistracionMasivaCore
{
    class Program
    {
        static void Main(string[] args)
        {
            var stream = File.OpenText(@"C:\Users\lbuedikman\source\repos\RegistracionMasivaCore\RegistracionMasivaCore\Formulario.txt");

            var json= stream.ReadToEnd();
            
            for(int i=0;i<=1000000; i++)
            {
                var ruta = @"http://localhost:5000/Registracion/RegistrarFormulario";
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(ruta);
                request.Method = "POST";
                request.ContentType = "application/json";
                request.ContentLength = json.Length;
                using (Stream webStream = request.GetRequestStream())
                using (StreamWriter requestWriter = new StreamWriter(webStream, System.Text.Encoding.ASCII))
                {
                    requestWriter.Write(json);
                }

                WebResponse webResponse = request.GetResponse();
                using (Stream webStream = webResponse.GetResponseStream() ?? Stream.Null)
                using (StreamReader responseReader = new StreamReader(webStream))
                {
                    var jsonresult = responseReader.ReadToEnd();
                    Console.Write(jsonresult);
                    Console.Write("----------------------------------------------------");
                }
            }
        }
    }
}
